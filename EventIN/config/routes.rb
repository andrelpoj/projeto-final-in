Rails.application.routes.draw do
  
  # Rotas de Transferencia
  get 'transfers/choice', to: 'transfers#choice', as: :transfer_choice
  post 'choice_create' => 'transfers#choice_create'
  
  # Rotas de Ranking
  get 'federacaos/ranking', to: 'federacaos#ranking', as: :ranking
  get 'ejs/ranking', to: 'ejs#ranking', as: :ranking_ejs
  
  # Rotas de escolha de palestras/workshops
  get 'workshops/choice', to: 'workshops#choice', as: :workshops_choice
  post 'workshops/submit', to: 'workshops#submit', as: :workshops_submit
  
  # Rotas de Materials
  get '/download_file', to: 'materials#download_file', as: :download_file
  
  # Rotas de escolha de quarto
  delete 'rooms/reservation', to: 'rooms#cancel_reservation', as: :cancel_reservation 
  get 'locals/choice', to: 'locals#choice', as: :local_choice
  get 'locals/rooms/:id', to: 'rooms#choice', as: :room_choice
  patch 'users/local/:id', to: 'users#reservate', as: :reservate
  
  # Recuperacao de senha
  get 'users/password_recover', to: 'users#password_recover', as: :password_recover
  post 'users/password_recover', to: 'users#set_password'
  
  resources :transfers
  resources :speakers
  resources :events
  resources :materials
  resources :workshops
  resources :rooms
  resources :locals
  resources :ejs
  resources :federacaos
  #resources :users
  
  
  # Rotas de UserController
  #get 'users/crud', to: 'users#crud', as: :crud
  get 'users', to: 'users#index', as: :users
  get 'users/new', to: 'users#new', as: :user_new
  post 'users/new', to: 'users#create'
  get 'users/:id', to: 'users#show', as: :user
  get 'users/edit/:id', to: 'users#edit', as: :user_edit
  patch 'users/edit/:id', to: 'users#update'
  delete 'users/destroy/:id', to: 'users#destroy', as: :user_destroy
  
  #Rotas de SessionsController
  get 'login', to: 'sessions#new', as: :login
  post 'login', to: 'sessions#create'
  delete 'logout',to: 'sessions#destroy', as: :logout

end
