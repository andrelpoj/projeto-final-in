# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170817020522) do

  create_table "ejs", force: :cascade do |t|
    t.string "name"
    t.integer "federacao_id"
    t.boolean "federada"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "count", default: 0
    t.index ["federacao_id"], name: "index_ejs_on_federacao_id"
  end

  create_table "events", force: :cascade do |t|
    t.datetime "begin_time"
    t.datetime "finish_time"
    t.datetime "begin_event_time"
    t.datetime "finish_event_time"
    t.integer "capacity"
    t.integer "occupied", default: 0
    t.string "local"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "federacaos", force: :cascade do |t|
    t.string "name"
    t.string "estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locals", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo"
  end

  create_table "materials", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "file"
    t.integer "workshop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["workshop_id"], name: "index_materials_on_workshop_id"
  end

  create_table "rooms", force: :cascade do |t|
    t.integer "number"
    t.integer "capacity"
    t.integer "occupied", default: 0
    t.boolean "available"
    t.integer "local_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["local_id"], name: "index_rooms_on_local_id"
  end

  create_table "speakers", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "cel"
    t.string "password_digest"
    t.string "profile_photo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transfers", force: :cascade do |t|
    t.string "old_name"
    t.string "old_cpf"
    t.string "old_email"
    t.string "old_cel"
    t.integer "user_id"
    t.datetime "transfer_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_transfers_on_user_id"
  end

  create_table "user_workshops", force: :cascade do |t|
    t.integer "user_id"
    t.integer "workshop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_workshops_on_user_id"
    t.index ["workshop_id"], name: "index_user_workshops_on_workshop_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "nick"
    t.string "cel"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.string "cpf"
    t.string "cep"
    t.integer "ej_id"
    t.string "cargo_ej"
    t.boolean "pos_junior"
    t.integer "federacao_id"
    t.string "cargo_federacao"
    t.integer "room_id"
    t.string "profile_photo"
    t.index ["ej_id"], name: "index_users_on_ej_id"
    t.index ["federacao_id"], name: "index_users_on_federacao_id"
    t.index ["room_id"], name: "index_users_on_room_id"
  end

  create_table "workshops", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.datetime "begin_time"
    t.datetime "finish_time"
    t.integer "capacity"
    t.integer "occupied", default: 0
    t.boolean "available"
    t.string "local"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "speaker_id"
    t.index ["speaker_id"], name: "index_workshops_on_speaker_id"
  end

end
