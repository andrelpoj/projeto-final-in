class CreateMaterials < ActiveRecord::Migration[5.1]
  def change
    create_table :materials do |t|
      t.string :title
      t.text :description
      t.string :link
      t.references :workshop, foreign_key: true

      t.timestamps
    end
  end
end
