class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.integer :number
      t.integer :capacity
      t.integer :occupied
      t.boolean :available
      t.references :local, foreign_key: true

      t.timestamps
    end
  end
end
