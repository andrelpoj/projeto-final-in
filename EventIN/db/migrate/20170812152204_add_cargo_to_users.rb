class AddCargoToUsers < ActiveRecord::Migration[5.1]
  def change
    
    add_reference :users, :ej, foreign_key: true
    add_column :users, :cargo_ej, :string
    
    add_column :users, :pos_junior, :boolean
    
    add_reference :users, :federacao, foreign_key: true
    add_column :users, :cargo_federacao, :string
  end
end
