class AddDefaultValueToRooms < ActiveRecord::Migration[5.1]
  def change
    change_column :rooms, :occupied, :integer, :default => 0
  end
end
