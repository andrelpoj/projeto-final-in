class CreateTransfers < ActiveRecord::Migration[5.1]
  def change
    create_table :transfers do |t|
      t.string :old_name
      t.string :old_cpf
      t.string :old_email
      t.string :old_cel
      t.references :user, foreign_key: true
      t.datetime :transfer_date

      t.timestamps
    end
  end
end
