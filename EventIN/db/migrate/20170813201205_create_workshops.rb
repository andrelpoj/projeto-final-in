class CreateWorkshops < ActiveRecord::Migration[5.1]
  def change
    create_table :workshops do |t|
      t.string :title
      t.text :description
      t.datetime :begin_time
      t.datetime :finish_time
      t.integer :capacity
      t.integer :occupied, :default => 0
      t.boolean :available
      t.string :local
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
