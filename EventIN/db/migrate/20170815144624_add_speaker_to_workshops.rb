class AddSpeakerToWorkshops < ActiveRecord::Migration[5.1]
  def change
     add_reference :workshops, :speaker, index: true
     remove_column :workshops, :user_id
  end
end
