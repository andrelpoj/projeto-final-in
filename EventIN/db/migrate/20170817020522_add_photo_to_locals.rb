class AddPhotoToLocals < ActiveRecord::Migration[5.1]
  def change
    add_column :locals, :photo, :string
  end
end
