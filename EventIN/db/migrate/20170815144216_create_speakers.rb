class CreateSpeakers < ActiveRecord::Migration[5.1]
  def change
    create_table :speakers do |t|
      t.string :name
      t.string :email
      t.string :cel
      t.string :password_digest
      t.string :profile_photo

      t.timestamps
    end
  end
end
