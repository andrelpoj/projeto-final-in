class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.datetime :begin_time
      t.datetime :finish_time
      t.datetime :begin_event_time
      t.datetime :finish_event_time
      t.integer :capacity
      t.integer :occupied, :default => 0
      t.string :local

      t.timestamps
    end
  end
end
