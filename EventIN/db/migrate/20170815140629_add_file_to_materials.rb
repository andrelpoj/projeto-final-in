class AddFileToMaterials < ActiveRecord::Migration[5.1]
  def change
    rename_column :materials, :link, :file
  end
end
