class AddCountToEjs < ActiveRecord::Migration[5.1]
  def change
    add_column :ejs, :count, :integer, :default => 0
  end
end
