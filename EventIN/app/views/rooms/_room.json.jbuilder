json.extract! room, :id, :number, :capacity, :occupied, :available, :local_id, :created_at, :updated_at
json.url room_url(room, format: :json)
