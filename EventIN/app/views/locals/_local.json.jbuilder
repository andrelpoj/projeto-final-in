json.extract! local, :id, :name, :address, :created_at, :updated_at
json.url local_url(local, format: :json)
