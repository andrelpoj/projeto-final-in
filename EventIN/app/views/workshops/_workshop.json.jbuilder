json.extract! workshop, :id, :title, :description, :begin_time, :finish_time, :capacity, :occupied, :available, :local, :user_id, :created_at, :updated_at
json.url workshop_url(workshop, format: :json)
