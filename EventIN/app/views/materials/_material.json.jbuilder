json.extract! material, :id, :title, :description, :link, :workshop_id, :created_at, :updated_at
json.url material_url(material, format: :json)
