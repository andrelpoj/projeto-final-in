json.extract! transfer, :id, :old_name, :old_cpf, :old_email, :old_cel, :user_id, :transfer_date, :created_at, :updated_at
json.url transfer_url(transfer, format: :json)
