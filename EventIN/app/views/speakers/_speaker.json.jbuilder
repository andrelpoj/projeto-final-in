json.extract! speaker, :id, :name, :email, :cel, :password_digest, :profile_photo, :created_at, :updated_at
json.url speaker_url(speaker, format: :json)
