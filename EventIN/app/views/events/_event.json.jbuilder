json.extract! event, :id, :begin_time, :finish_time, :begin_event_time, :finish_event_time, :capacity, :occupied, :local, :created_at, :updated_at
json.url event_url(event, format: :json)
