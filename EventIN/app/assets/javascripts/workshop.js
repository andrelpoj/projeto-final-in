$(document).ready(function(){
  var adicionar = [];
  var remover = [];

  $('.inscricao').click(function(){    
    //na vdd e um release entao e o contrario do que voce faria
    
    id_workshop = $(this).val();
    
    
    if($(this).prop("checked")){
      r = remover.indexOf(id_workshop)
      if (r != -1){
        remover.splice(r,1);
      }
      else{
        adicionar.push(id_workshop);
      }
    }
    else{
      a = adicionar.indexOf(id_workshop)
      if (a != -1){
        adicionar.splice(a,1);
      }
      else{
        remover.push(id_workshop);
      }
    }
  });
  
  $('.btn_inscrever').click(function(){
    $.ajax({
            url:'../workshops/submit',
            method: 'POST',
            data: {
                listaAdicionar: adicionar,
                listaRemover: remover
            }
        }).done(function(retorno){
            alert("Operação concluída com sucesso!!");
            adicionar = [];
            remover = [];
        }).fail(function(retorno){
            alert("Ocorreu um erro!")
    })
    
  });

});
