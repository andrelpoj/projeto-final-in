class Ej < ApplicationRecord
  belongs_to :federacao
  
  #alias_attribute :ej_name, :name
  #alias_attribute :ej_id, :id
  
  has_many :users, dependent: :destroy
  
end
