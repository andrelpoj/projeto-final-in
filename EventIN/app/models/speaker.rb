class Speaker < ApplicationRecord
  mount_uploader :profile_photo, PictureUploader
  
  has_many :workshops
end
