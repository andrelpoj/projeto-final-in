class User < ApplicationRecord
    mount_uploader :profile_photo, PictureUploader
    
    has_secure_password
    
    belongs_to :ej;
    belongs_to :federacao;
    belongs_to :room, optional: true; #posso criar um usuario antes dele decidir onde vai ficar
    
    has_many :user_workshops, dependent: :destroy; #um usuario pode se inscrever em diversas palestras/workshops 
    has_one :transfer;
    
    validates :name, length: { in: 2..50, presence: true}
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :email, length: { in: 10..50}, format: { with: VALID_EMAIL_REGEX }
    #allow_nil permite que voce nao precise redigitar a senha quando editar o usuario
    validates :password, length: { in: 3..8, presence: true, allow_nil: true }
    validates :nick,  length: { in: 2..50, presence: true}
    
    VALID_CPF_REGEX = /[0-9]{3}.?[0-9]{3}.?[0-9]{3}-?[0-9]{2}/
    validates :cpf, format: { with: VALID_CPF_REGEX }, uniqueness: true
        
    VALID_CEP_REGEX = /[0-9]{2}.?[0-9]{3}-?[0-9]{3}/
    validates :cep, format: { with: VALID_CEP_REGEX }
    
    validate :cpf_format
    validate :cep_format
    
    after_create :deliver_mail
    
    def deliver_mail
        UserMailer.welcome_email(self).deliver_now
    end
    
    #VALID_CEL_REGEX = /[0-9]{2}[0-9]{5}-?[0-9]{4}/
    #validates :cel, format: { with: VALID_CEL_REGEX}
    
    def cpf_format
        cpf.gsub!("-","")
        cpf.gsub!(".","")
    end
    
    def cep_format
        cep.gsub!("-","")
        cep.gsub!(".","")
    end
    
end
