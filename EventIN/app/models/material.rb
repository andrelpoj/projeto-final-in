class Material < ApplicationRecord
  mount_uploader :file, FileUploader
  
  belongs_to :workshop
end
