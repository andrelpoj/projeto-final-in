class Workshop < ApplicationRecord
  belongs_to :speaker #palestrante
  
  has_many :materials
  
  has_many :user_workshops; #uma palestra/workshop pode ter diversos usuarios inscritos
end
