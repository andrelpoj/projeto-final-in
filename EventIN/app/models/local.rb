class Local < ApplicationRecord
  mount_uploader :photo, PictureUploader
  
  has_many :rooms;
end
