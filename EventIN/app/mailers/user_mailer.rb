class UserMailer < ApplicationMailer
  
  def welcome_email(user)
    @user = user
    @url  = 'https://projeto-final-andrelpoj.c9users.io/login'
    mail(to: @user.email, subject: 'Confirmação de cadastro')
  end
  
  def password_recover_email(user,password)
    @user = user
    @senha = password
    @url  = 'https://projeto-final-andrelpoj.c9users.io/login'
    mail(to: @user.email, subject: 'Recuperação de senha')
  end
  
  
end
