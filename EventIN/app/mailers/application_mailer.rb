class ApplicationMailer < ActionMailer::Base
  default from: 'zackalpdj@gmail.com'
  layout 'mailer'
end
