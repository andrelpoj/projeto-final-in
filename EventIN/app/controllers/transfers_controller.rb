class TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :edit, :update, :destroy]
  
  before_action :non_logged_user, except: [:show]
  
  before_action :non_admin, except: [:choice,:choice_create]
  
  def choice
    @transfer = Transfer.new
    #@user = User.new
    
    #É necessario passar as federacoes e ejs para o dropdown
    @federacaos = Federacao.all
    @ejs = Ej.all
  end
  
  def choice_create
    
    #aux = [ params[:transfer][:old_name], params[:transfer][:old_cpf], params[:transfer][:old_email], params[:transfer][:old_cel] ]
    #params[:transfer].delete :old_name
    #params[:transfer].delete :old_cpf
    #params[:transfer].delete :old_email
    #params[:transfer].delete :old_cel
    
    @user = User.new(:name => params[:transfer][:name], :email => params[:transfer][:email], :cel => params[:transfer][:cel], :cep => params[:transfer][:cep], :nick => params[:transfer][:nick], :cpf => params[:transfer][:cpf], :pos_junior => params[:transfer][:pos_junior], :federacao_id => params[:transfer][:federacao_id], :cargo_federacao => params[:transfer][:cargo_federacao], :ej_id => params[:transfer][:ej_id], :cargo_ej => params[:transfer][:cargo_ej], :password => params[:transfer][:password], :password_confirmation => params[:transfer][:password_confirmation])
    
    
    if @user.save
            #UserMailer.welcome_email(@user).deliver_now
            
            ej = Ej.find_by_id(@user.ej_id)
            ej.count = ej.count + 1
            ej.save
            
    else
      render :choice
    end
    
    @transfer = Transfer.new(:old_name => params[:transfer][:old_name], :old_cpf => params[:transfer][:old_cpf],
                              :old_email => params[:transfer][:old_email], :old_cel => params[:transfer][:old_cel],
                              :user_id => @user.id, :transfer_date => DateTime.now)
    
    
    respond_to do |format|
      if @transfer.save
        
        #user = User.find_by_id(current_user.id).destroy
        user = User.find_by_id(current_user.id)
        
        #if user
          e = Ej.find_by_id(@user.ej_id)
          e.count = e.count - 1
          e.save 
          
          #Transfere a reserva do quarto  
          @user.room_id = user.room_id
          @user.save
          
          
          #Deleta a inscrição nas palestras
          userWorkshop = UserWorkshop.where(:user_id => user.id)
          
          userWorkshop.where(:user_id => user.id).each do |relation|
            workshop = Workshop.find_by_id(relation.workshop_id)
            workshop.update(:occupied => workshop.occupied - 1)
            workshop.update(:available => true)
          end
          
          userWorkshop.destroy_all
          
        #end
        
        #Preciso deletar a transferencia que aponta para o usuario atual, caso ela exista
        Transfer.where(:user_id => current_user.id).destroy_all
        
        #preciso deslogar do usuario antes de destruir
        log_out
        user.destroy
        
        format.html { redirect_to @transfer, notice: 'Transfer was successfully created.' }
        format.json { render :show, status: :created, location: @transfer }
      else
        format.html { render :new }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # GET /transfers
  # GET /transfers.json
  def index
    #@transfers = Transfer.all
    @transfers = Transfer.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /transfers/1
  # GET /transfers/1.json
  def show
  end

  # GET /transfers/new
  def new
    @transfer = Transfer.new
  end

  # GET /transfers/1/edit
  def edit
  end

  # POST /transfers
  # POST /transfers.json
  def create
    @transfer = Transfer.new(transfer_params)

    respond_to do |format|
      if @transfer.save
        format.html { redirect_to @transfer, notice: 'Transfer was successfully created.' }
        format.json { render :show, status: :created, location: @transfer }
      else
        format.html { render :new }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transfers/1
  # PATCH/PUT /transfers/1.json
  def update
    respond_to do |format|
      if @transfer.update(transfer_params)
        format.html { redirect_to @transfer, notice: 'Transfer was successfully updated.' }
        format.json { render :show, status: :ok, location: @transfer }
      else
        format.html { render :edit }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transfers/1
  # DELETE /transfers/1.json
  def destroy
    @transfer.destroy
    respond_to do |format|
      format.html { redirect_to transfers_url, notice: 'Transfer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer
      @transfer = Transfer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transfer_params
      params.require(:transfer).permit(:old_name, :old_cpf, :old_email, :old_cel,
                      :name, :email, :password, :password_confirmation, 
                      :nick, :cel, :cpf,:cep,:ej_id,:cargo_ej,:pos_junior,:federacao_id,
                      :cargo_federacao)
    end
    
    #def user_params
    #    @user_params = params.require(:user).permit(:name, :email, :password, :password_confirmation, 
    #                                    :nick, :cpf,:cep,:ej_id,:cargo_ej,:pos_junior,:federacao_id,
    #                                    :cargo_federacao, :profile_photo)
    #end
    
end
