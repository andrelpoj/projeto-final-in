class FederacaosController < ApplicationController
  before_action :non_logged_user
  
  before_action :set_federacao, only: [:show, :edit, :update, :destroy]
  
  before_action :non_admin, except: [:ranking]
  
  
  def ranking
    #nao consigo renomear as colunas para selecionar após o join
    
    top = 10 #numero de federacoes no top
    
    @federacaos = []
    
    @num = []
    @name = []
    Federacao.all.each do |federacao|
      @name.append federacao.name
      @num.append Ej.where("federacao_id = ?", federacao.id).sum(:count)
    end
    
    i = 0 
    tam = @num.length
    while (i<top)&&(i<tam) do
      
      num = @num.max
      index = @num.rindex(num)
      name = @name[index]
      
      @federacaos.append({:name => name,:num => num})
      
      @num.delete(num)
      @name.delete(name)
      i = i + 1
    end
    
  end
  
  # GET /federacaos
  # GET /federacaos.json
  def index
    #@federacaos = Federacao.all
    @federacaos = Federacao.paginate(:page => params[:page], :per_page => 5)
  end

  # GET /federacaos/1
  # GET /federacaos/1.json
  def show
  end

  # GET /federacaos/new
  def new
    @federacao = Federacao.new
  end

  # GET /federacaos/1/edit
  def edit
  end

  # POST /federacaos
  # POST /federacaos.json
  def create
    @federacao = Federacao.new(federacao_params)

    respond_to do |format|
      if @federacao.save
        format.html { redirect_to @federacao, notice: 'Federacao was successfully created.' }
        format.json { render :show, status: :created, location: @federacao }
      else
        format.html { render :new }
        format.json { render json: @federacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /federacaos/1
  # PATCH/PUT /federacaos/1.json
  def update
    respond_to do |format|
      if @federacao.update(federacao_params)
        format.html { redirect_to @federacao, notice: 'Federacao was successfully updated.' }
        format.json { render :show, status: :ok, location: @federacao }
      else
        format.html { render :edit }
        format.json { render json: @federacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /federacaos/1
  # DELETE /federacaos/1.json
  def destroy
    @federacao.destroy
    respond_to do |format|
      format.html { redirect_to federacaos_url, notice: 'Federacao was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_federacao
      @federacao = Federacao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def federacao_params
      params.require(:federacao).permit(:name, :estado)
    end
end
