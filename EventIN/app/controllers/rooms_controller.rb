class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  
  before_action :non_admin, except: [:choice,:show,:cancel_reservation]
  
  before_action :non_logged_user, only: [:choice,:show,:cancel_reservation]
  before_action :no_room?, only: [:choice]
  
  def cancel_reservation
    room_id = current_user.room_id
    current_user.room_id = nil
    current_user.save
    
    room = Room.find_by_id(room_id)
    room.update(:occupied => room.occupied - 1 )
    room.update(:available => true)
    
    #flash[:notice] = "Você cancelou sua reserva no quarto!"
    redirect_to local_choice_path
  end
  
  def no_room?
    if current_user.room_id
      #flash[:notice] = "Você está alocado nesse quarto!"
      redirect_to room_path(room_id)
    end
  end
  
  
  def choice
    @rooms = Room.where(local_id: params[:id], available: true)
    
    #current_user
  end
  
  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = Room.all
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
  end

  # GET /rooms/new
  def new
    @room = Room.new
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = Room.new(room_params)

    respond_to do |format|
      if @room.save
        format.html { redirect_to @room, notice: 'Room was successfully created.' }
        format.json { render :show, status: :created, location: @room }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to @room, notice: 'Room was successfully updated.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room.destroy
    respond_to do |format|
      format.html { redirect_to rooms_url, notice: 'Room was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:number, :capacity, :occupied, :available, :local_id)
    end
end
