class UsersController < ApplicationController
before_action :set_user, only: [:show, :edit, :destroy, :update]
    
    before_action :logged_user, only: [:new, :create]
    before_action :non_logged_user, except: [:new, :create,:password_recover,:set_password]
    
    #before_action :verify_date
    
    before_action :correct_user_or_admin,only: [:edit,:update,:destroy]
    
    before_action :set_federacaos, only: [:new,:create,:edit]
    before_action :set_ejs, only: [:new,:create,:edit]
    
    def verify_date
        d = DateTime.now
        event = Event.first
        if d < event.begin_time
           flash[:notice] = "As inscrições ainda não foram liberadas" 
           redirect_to login_path
        end
    end
    
    def reservate
        room = Room.find_by_id(params[:id])
        current_user.room_id = room.id
        
        if current_user.save
            flash[:notice] = "Sua reserva foi efetuada"
            room.update(:occupied => room.occupied + 1 )
            
            if room.occupied == room.capacity
               room.update(:available => false) 
            end
        else
            flash[:error] = "Sua reserva nao foi efetuada"
        end
        
        redirect_to local_choice_path
    end
    
    def set_federacaos
       @federacaos = Federacao.all.order("name") 
    end
    
    def set_ejs
       @ejs = Ej.all.order("name") 
    end
    
    
    def password_recover
    end
    
    def set_password
       password = [*('A'..'Z')].sample(8).join
        
        u = User.find_by(email: params[:session][:email])
        #u.password = password
        #u.password_confirmation = password
        
        #if u.update
            u.update(:password => password, :password_confirmation => password)
            UserMailer.password_recover_email(u,password).deliver_now
        #end
        redirect_to login_path 
    end
    
    #def generate_password
    #    require 'securerandom'
    #    random_string = SecureRandom.hex
    #end
    
    def crud
        @users = User.paginate(:page => params[:page], :per_page => 5)
    end
    
    def new 
        @user = User.new
    end
    
    def create
        @user = User.new(user_params)
        if @user.save
            
            #UserMailer.welcome_email(@user).deliver_now
            
            e = Ej.find_by_id(@user.ej_id)
            e.count = e.count + 1
            e.save
            
            event = Event.first
            event.update(:occupied => event.occupied + 1)
            
            redirect_to user_path(@user)
        else
            render :new
        end
    end
    
    def show
        @ejs = Ej.where("count>0").order("count DESC").limit(5);
    end
    
    def edit
    end
    
    def update
        if @user.update_attributes(user_params)
            redirect_to user_path(@user)
        else
            render :edit
        end
    end
    
    def index
        #Variaveis com @ sao variaveis de instancia
        #elas ficam disponiveis para as views
        #@users = User.all
        @users = User.paginate(:page => params[:page], :per_page => 5)
        
    end
    
    def destroy
        if @user == current_user
            log_out
        end
        
        if @user.destroy
            e = Ej.find_by_id(@user.ej_id)
            e.count = e.count - 1
            e.save 
            
            event = Event.first
            event.update(:occupied => event.occupied - 1)
            
            
            #tira a reserva do quarto
            room = Room.find_by_id(@user.room_id)
            room.update(:occupied => room.occupied - 1 )
            room.update(:available => true)
            
            #Deleta a inscrição nas palestras
            UserWorkshop.where(:user_id => user.id).each do |relation|
                workshop = Workshop.find_by_id(relation.workshop_id)
                workshop.update(:occupied => workshop.occupied - 1)
                workshop.update(:available => true)
            end
            
        end
        
        
        
        redirect_to users_path
    end
    
    private
    
    def set_user
       @user = User.find(params[:id]) 
    end
    
    def user_params
        @user_params = params.require(:user).permit(:name, :email, :password, :password_confirmation, 
                                        :nick, :cel, :cpf,:cep,:ej_id,:cargo_ej,:pos_junior,:federacao_id,
                                        :cargo_federacao, :profile_photo)
    end
    
    def correct_user_or_admin
       if !(current_user == @user || current_user.admin)
           redirect_to user_path(current_user)
       end
    end
end
